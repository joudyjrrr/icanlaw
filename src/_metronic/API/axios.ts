import axios from "axios";
export const DEVELOPMENT_BASE_URL = process.env.REACT_APP_API_URL;
export const API_BASE_URL = `${DEVELOPMENT_BASE_URL}`;
const DeliveryApiInstances = axios.create({
    baseURL: API_BASE_URL,
});
DeliveryApiInstances.interceptors.request.use((config) => {
    const token = localStorage.getItem("kt-auth-react-v")
    if (config.headers) {
        config.headers.Authorization = ` Bearer ${localStorage.getItem("t")}`
    }
    return config
})
export default DeliveryApiInstances;

