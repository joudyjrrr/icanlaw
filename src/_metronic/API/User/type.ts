import { IMSelect } from "../Task/type";

export interface IUser {
    id?:number
    name : string;
    email:string;
    username : string;
    password:string;
    password_confirmation:string;
}
export interface IUserForm extends IUser{
    role:IMSelect;
} 
export interface IUseSend extends IUser{
    role_id:number;
} 
