import { useMutation, useQuery } from "react-query"
import DeliveryApiInstances from "../axios"
import { IUseSend } from "./type"
import { toast } from "react-toastify"

const getUsers = async () => {
    // console.log(params.Query)
    const { data } = await DeliveryApiInstances.get<any>('/users')
    return data
}
const useQueryUsersAll = () => {
    const queryResult = useQuery({
        queryKey: ['get-users'],
        queryFn: async () => {
            const data = await getUsers()
            return data
        },
    })
    return queryResult
}
const getRoles = async () => {
    // console.log(params.Query)
    const { data } = await DeliveryApiInstances.get<any>('/users/create')
    return data
}
const useQueryRoles = () => {
    const queryResult = useQuery({
        queryKey: ['get-role'],
        queryFn: async () => {
            const data = await getRoles()
            return data
        },
        select: (data) =>
            data.map((user: any) => ({
                value: user.id,
                label: user.name,
            })),
    })
    return queryResult
}
const NewUser = async (data: IUseSend) => {
    return await DeliveryApiInstances.post('/users/store', data)
}

const NewUserQuery = () => {
    const queryResult = useMutation({
        mutationKey: ['set-user'],
        mutationFn: NewUser,
        onSuccess(data: any) {
            toast.success(data.data.message)
        },
        onError(error: any) {
            toast.error(error.response.data.message)
        },
    })
    return queryResult
}
const UbdateUser = async (data: IUseSend) => {
    return await DeliveryApiInstances.post(`/users/update/${data.id}`, data)
}

const UbdateUserQuery = () => {
    const queryResult = useMutation({
        mutationKey: ['ubdate-user'],
        mutationFn: UbdateUser,
        onSuccess(data: any) {
            toast.success(data.data.message)
        },
        onError(error: any) {
            toast.error(error.response.data.message)
        },
    })
    return queryResult
}
const getUserBYId = async (id: number) => {
    // console.log(params.Query)
    const { data } = await DeliveryApiInstances.get<any>(`/users/edit/${id}`)
    return data
}
const useQueryUserById = (id: number) => {
    const queryResult = useQuery({
        queryKey: ['get-user-id', id],
        queryFn: async () => {
            const data = await getUserBYId(id)
            return data
        },
        enabled: !!id,
    })
    return queryResult
}
const UserApi = {
    useQueryUsersAll,
    useQueryRoles,
    NewUserQuery,
    useQueryUserById,
    UbdateUserQuery

}
export default UserApi