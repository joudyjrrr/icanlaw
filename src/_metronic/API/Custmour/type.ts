export interface GetCusmour {
    data: {
        id: number;
        name_ar: string;
        name_en: string;
        number_of_lawsuits: number;
        user: string
    }
    success : true
}
export interface IDataFormNames {
    id?:number;
    name_en : string;
    name_ar : string
}