import { useMutation, useQuery } from "react-query"
import DeliveryApiInstances from "../axios"
import { IDataFormNames } from "./type"
import { toast } from "react-toastify"


const getCustmourAdj = async () => {
    // console.log(params.Query)
    const { data } = await DeliveryApiInstances.get<any>('/customer-adjectives')
    return data
}
const useQueryCustmourAdj = () => {
    const queryResult = useQuery({
        queryKey: ['get-adj'],
        queryFn: async () => {
            const data = await getCustmourAdj()
            return data
        },
    })
    return queryResult
}
const NewCustmourAdj = async (data: IDataFormNames) => {
    return await DeliveryApiInstances.post('/customer-adjectives/store', data)
}

const NewCustmourAdjQuery = () => {
    const queryResult = useMutation({
        mutationKey: ['set-custmour'],
        mutationFn: NewCustmourAdj,
        onSuccess(data: any) {
            toast.success(data.data.message)
        },
        onError(error: any) {
            toast.error(error.response.data.message)
        },
    })
    return queryResult
}
const UbdateCustmourAdj = async (data: IDataFormNames) => {
    return await DeliveryApiInstances.post(`/customer-adjectives/update/${data.id}`, data)
}

const UbdateCustmourAdjQuery = () => {
    const queryResult = useMutation({
        mutationKey: ['set-custmour-adj-ubdate'],
        mutationFn: UbdateCustmourAdj,
        onSuccess(data: any) {
            toast.success(data.data.message)
        },
        onError(error: any) {
            toast.error(error.response.data.message)
        },
    })
    return queryResult
}
const getCustmourAdjBYId = async (id: number) => {
    // console.log(params.Query)
    const {data} = await DeliveryApiInstances.get<any>(`/customer-adjectives/edit/${id}`)
    return data
  }
  const useQueryCustAdjById = (id: number) => {
    const queryResult = useQuery({
      queryKey: ['get-adj-id', id],
      queryFn: async () => {
        const data = await getCustmourAdjBYId(id)
        return data
      },
      enabled: !!id,
    })
    return queryResult
  }
const CustAdjApi = {
    useQueryCustmourAdj,
    NewCustmourAdjQuery,
    UbdateCustmourAdjQuery,
    useQueryCustAdjById,

}
export default CustAdjApi