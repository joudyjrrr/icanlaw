import {useMutation, useQuery} from 'react-query'
import DeliveryApiInstances from '../axios'

import {toast} from 'react-toastify'
import { IDataFormNames } from './type'

const getCustmourType = async () => {
  // console.log(params.Query)
  const {data} = await DeliveryApiInstances.get<any>('/customer-types')
  return data
}
const useQueryCustmourType = () => {
  const queryResult = useQuery({
    queryKey: ['get-custmour'],
    queryFn: async () => {
      const data = await getCustmourType()
      return data
    },
  })
  return queryResult
}
const NewCustmourType = async (data: IDataFormNames) => {
    return await DeliveryApiInstances.post('/customer-types/store', data)
  }
  
  const NewCustmourTypeQuery = () => {
    const queryResult = useMutation({
      mutationKey: ['set-custmour'],
      mutationFn: NewCustmourType,
      onSuccess(data: any) {
        toast.success(data.data.message)
      },
      onError(error: any) {
        toast.error(error.response.data.message)
      },
    })
    return queryResult
  }
  const UbdateCustmourType = async (data: IDataFormNames) => {
    return await DeliveryApiInstances.post(`/customer-types/update/${data.id}`, data)
  }
  
  const UbdateCustmourTypeQuery = () => {
    const queryResult = useMutation({
      mutationKey: ['set-custmour-ubdate'],
      mutationFn: UbdateCustmourType,
      onSuccess(data: any) {
        toast.success(data.data.message)
      },
      onError(error: any) {
        toast.error(error.response.data.message)
      },
    })
    return queryResult
  }
  const getCustmourBYId = async (id: number) => {
    // console.log(params.Query)
    const {data} = await DeliveryApiInstances.get<any>(`/customer-types/edit/${id}`)
    return data
  }
  const useQueryCustById = (id: number) => {
    const queryResult = useQuery({
      queryKey: ['get-tsak-id', id],
      queryFn: async () => {
        const data = await getCustmourBYId(id)
        return data
      },
      enabled: !!id,
    })
    return queryResult
  }
const CustTypeApi = {
  useQueryCustmourType,
  NewCustmourTypeQuery,
  useQueryCustById,
  UbdateCustmourTypeQuery
}
export default CustTypeApi
