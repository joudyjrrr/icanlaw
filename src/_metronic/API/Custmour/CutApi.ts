import { useQuery } from "react-query"
import DeliveryApiInstances from "../axios"




const getCustmour = async () => {
    // console.log(params.Query)
    const { data } = await DeliveryApiInstances.get<any>('/customers')
    return data
}
const useQueryCustmour = () => {
    const queryResult = useQuery({
        queryKey: ['get-adj'],
        queryFn: async () => {
            const data = await getCustmour()
            return data
        },
    })
    return queryResult
}

const CutApi = {
    useQueryCustmour
}
export default CutApi