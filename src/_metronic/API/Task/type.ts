
export interface Task {
  description: string
  start_date: string
  completion_date: string
  attached?: File
  note: string
}
export interface INewTaskForm extends Task {
  task_status: IMSelect,
  users: IMSelect[]
}
export interface INewTaskSend extends Task {
  task_status_id: number,
  users: number[]
}
export interface IMSelect {
  label: string
  value: number|null
}
export interface IUbdateTask  extends Task {
  task_status_id: number,
  users: number[],
  id : number
}