import {useMutation, useQuery} from 'react-query'
import DeliveryApiInstances from '../axios'
import {INewTaskSend, IUbdateTask} from './type'
import {toast} from 'react-toastify'

const getUsers = async () => {
  // console.log(params.Query)
  const {data} = await DeliveryApiInstances.get<any>('/tasks/create')
  return data
}
const getTaskBYId = async (id: number) => {
  // console.log(params.Query)
  const {data} = await DeliveryApiInstances.get<any>(`/tasks/show/${id}`)
  return data
}
const useQueryTasksById = (id: number) => {
  const queryResult = useQuery({
    queryKey: ['get-tsak-id', id],
    queryFn: async () => {
      const data = await getTaskBYId(id)
      return data
    },
    enabled: !!id,
  })
  return queryResult
}
const getTask = async (page: number) => {
  // console.log(params.Query)
  const {data} = await DeliveryApiInstances.get<any>(`/tasks?page=${page}`)
  return data
}
const DeleteTask = async (id: number) => {
  // console.log(params.Query)
  const {data} = await DeliveryApiInstances.post<any>(`/tasks/delete/${id}`)
  return data
}
const useQueryTasksAll = (page: number) => {
  const queryResult = useQuery({
    queryKey: ['get-tsak-all', page],
    queryFn: async () => {
      const data = await getTask(page)
      return data
    },
  })
  return queryResult
}
const useQueryUsers = () => {
  const queryResult = useQuery({
    queryKey: ['get-users'],
    queryFn: async () => {
      const data = await getUsers()
      return data
    },
    select: (data) =>
      data.users.map((user: any) => ({
        value: user.id,
        label: user.name,
      })),
  })
  return queryResult
}
const useQueryStatus = () => {
  const queryResult = useQuery({
    queryKey: ['get-status'],
    queryFn: async () => {
      const data = await getUsers()
      return data
    },
    select: (data) =>
      data.status.map((st: any) => ({
        value: st.id,
        label: st.name_en,
      })),
  })
  return queryResult
}

const NewTask = async (data: INewTaskSend) => {
  return await DeliveryApiInstances.post('/tasks/store', data)
}

const NewTaskQuery = () => {
  const queryResult = useMutation({
    mutationKey: ['set-task'],
    mutationFn: NewTask,
    onSuccess(data: any) {
      toast.success(data.data.message)
    },
    onError(error: any) {
      toast.error(error.response.data.message)
    },
  })
  return queryResult
}
const UbdateTask = async (data: IUbdateTask) => {
  return await DeliveryApiInstances.post(`/tasks/update/${data?.id}`, data)
}
const UbdateTaskQuery = () => {
  const queryResult = useMutation({
    mutationKey: ['ubdate-task'],
    mutationFn: UbdateTask,
    onSuccess(data: any) {
      toast.success(data.data.message)
    },
    onError(error: any) {
      toast.error(error.response.data.message)
    },
  })
  return queryResult
}
const TaskApi = {
  useQueryUsers,
  NewTaskQuery,
  useQueryStatus,
  useQueryTasksAll,
  DeleteTask,
  useQueryTasksById,
  UbdateTaskQuery
}
export default TaskApi
