/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import {useIntl} from 'react-intl'
import {AsideMenuItemWithSub} from './AsideMenuItemWithSub'
import {AsideMenuItem} from './AsideMenuItem'

export function AsideMenuMain() {
  const intl = useIntl()

  return (
    <>
      <AsideMenuItem
        to='/dashboard'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({id: 'MENU.DASHBOARD'})}
        fontIcon='bi-app-indicator'
      />
      <AsideMenuItem
        to='/tasks'
        icon='/media/icons/duotune/art/art002.svg'
        title={`Tasks`}
        fontIcon='bi-app-indicator'
      />
      <AsideMenuItemWithSub
        to='/customer'
        title='Customers'
        icon='/media/icons/duotune/communication/com006.svg'
        fontIcon='bi-person'
      >
        <AsideMenuItem to='/customerType' title='CustomerType' hasBullet={true} />
        <AsideMenuItem to='/customeradj' title='CustomerAdjectives' hasBullet={true} />
        <AsideMenuItem to='/customer' title='Customer' hasBullet={true} />
      </AsideMenuItemWithSub>
    </>
  )
}
