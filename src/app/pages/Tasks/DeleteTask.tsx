import React from 'react'
import {useState} from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import {useMutation} from 'react-query'
import TaskApi from '../../../_metronic/API/Task/TaskApi'
import {toast} from 'react-toastify'
import {KTSVG, toAbsoluteUrl} from '../../../_metronic/helpers'
import {ClipLoader} from 'react-spinners'
import DeliveryApiInstances from '../../../_metronic/API/axios'
const DeleteTask = ({
  id,
  setId,
  refetch,
  to,
}: {
  to: string
  setId: (id: number) => void
  id: number
  refetch: () => void
}) => {
  const [show, setShow] = useState(false)
  const DeleteTask = async (id: number) => {
    // console.log(params.Query)
    const {data} = await DeliveryApiInstances.post<any>(`/${to}/delete/${id}`)
    return data
  }
  const {mutate, isLoading} = useMutation({
    mutationFn: DeleteTask,
    onSuccess: () => {
      toast.success('Deleted.....')
      setShow(false)
    },
    onError: (error: any) => {
      toast.error(error)
    },
  })
  const handelDelete = () => {
    mutate(id)
  }
  return (
    <>
      <a
        onClick={() => {
          setShow(true)
          setId(id)
        }}
        href='#'
        className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
      >
        <KTSVG path='/media/icons/duotune/general/gen027.svg' className='svg-icon-3' />
      </a>
      <Modal show={show} onHide={() => setShow(false)} className='mt-20'>
        <Modal.Header closeButton>
          <Modal.Title>Do You want to delete it ..?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button variant='secondary' onClick={() => setShow(false)}>
            Cancel
          </Button>
          <Button disabled={isLoading} variant='primary' onClick={handelDelete}>
            {isLoading ? <ClipLoader color='#fff' size={20} /> : 'Yes'}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default DeleteTask
