import React, {useEffect} from 'react'
import {KTSVG} from '../../../_metronic/helpers'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import TaskApi from '../../../_metronic/API/Task/TaskApi'
import Select from 'react-select'
import {Button, Modal} from 'react-bootstrap'
import {Formik, Field, useFormikContext, useFormik} from 'formik'
import * as Yup from 'yup'
import {ClipLoader} from 'react-spinners'

const initialValues = {
  id: 0,
  description: '',
  start_date: new Date(),
  completion_date: new Date(),
  note: '',
  users: [],
  task_status: {
    label: '',
    value: 0,
  },
}

const NewTasks = ({
  open,
  setOpen,
  id,
  setId,
}: {
  open: boolean
  setOpen: (arg: boolean) => void
  id: number
  setId: (arg: number) => void
}) => {
  const TaskSchema = Yup.object().shape({
    description: Yup.string().required('Description is required'),
  })

  const {data: TaskData, isLoading: loadingData} = TaskApi.useQueryTasksById(id)

  useEffect(() => {
    if (TaskData) {
      formik.setFieldValue('id', TaskData.id)
      formik.setFieldValue('description', TaskData.description)
      formik.setFieldValue('note', TaskData.note)
      formik.setFieldValue('start_date', new Date(TaskData.start_date))
      formik.setFieldValue('completion_date', new Date(TaskData.completion_date))
      formik.setFieldValue(
        'users',
        TaskData.users.map((d: any) => {
          return {
            value: d.id,
            label: d.name,
          }
        })
      )
      formik.setFieldValue('task_status', {
        value: TaskData.task_status.id,
        label: TaskData.task_status.name_en,
      })
    }
  }, [TaskData])

  const {data: users} = TaskApi.useQueryUsers()
  const {data: status} = TaskApi.useQueryStatus()

  const convertDateFormat = (date: any) => {
    const newDate = new Date(date)
    const year = newDate.getFullYear()
    const month = newDate.getMonth() + 1
    const day = newDate.getDate()
    return `${year}-${month}-${day}`
  }
  const {mutate, isLoading} = TaskApi.NewTaskQuery()
  const {mutate: mutateUbdate, isLoading: loadingUbdate} = TaskApi.UbdateTaskQuery()
  const {...formik} = useFormik({
    initialValues,
    validationSchema: TaskSchema,
    onSubmit: async (values, {resetForm}) => {
      if (id) {
        mutateUbdate(
          {
            id: id,
            description: values.description,
            note: values.note,
            start_date: convertDateFormat(values.start_date),
            completion_date: convertDateFormat(values.completion_date),
            users: values.users.map((d: any) => d.value),
            task_status_id: values.task_status.value,
          },
          {
            onSuccess: () => {
              setOpen(false)
              resetForm()
            },
          }
        )
      } else {
        await mutate(
          {
            description: values.description,
            note: values.note,
            start_date: convertDateFormat(values.start_date),
            completion_date: convertDateFormat(values.completion_date),
            users: values.users.map((d: any) => d.value),
            task_status_id: values.task_status.value,
          },
          {
            onSuccess: () => {
              setOpen(false)
              resetForm()
            },
          }
        )
      }
    },
  })

  return (
    <>
      <Modal show={open} onHide={() => setOpen(false)}>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title fs-6 fw-bolder mb-3'>New Tasks</h5>
          </div>
          {loadingData ? (
            <div className='d-flex justify-content-center align-items-center mx-auto w-100'>
              <ClipLoader color='#36d7b7' size={80} />
            </div>
          ) : (
            <div className='modal-body'>
              <form onSubmit={formik.handleSubmit}>
                <div className='d-flex gap-5 justify-content-center mx-auto'>
                  <div className='mb-10'>
                    <label className='form-label fs-6 fw-bolder mb-3'>Description</label>
                    <input
                      type='text'
                      name='description'
                      className='form-control form-control-lg form-control-solid '
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                    {formik.touched.description && formik.errors.description && (
                      <div className='text-danger'>{formik.errors.description}</div>
                    )}
                  </div>
                  <div className='mb-10'>
                    <label className='form-label fs-6 fw-bolder mb-3'>Start Date</label>
                    <DatePicker
                      name='start_date'
                      className='form-control form-control-lg form-control-solid py-3'
                      selected={formik.values.start_date}
                      onChange={(newDate) => {
                        formik.setFieldValue('start_date', newDate)
                      }}
                    />
                  </div>
                </div>
                <div className='d-flex gap-5'>
                  <div className='mb-10'>
                    <label className='form-label fs-6 fw-bolder mb-3'>End Date</label>
                    <DatePicker
                      name='completion_date'
                      className='form-control form-control-lg form-control-solid py-3'
                      selected={formik.values.completion_date}
                      onChange={(newDate) => {
                        formik.setFieldValue('completion_date', newDate)
                      }}
                    />
                  </div>
                  <div className='mb-10'>
                    <label className=' exampleFormControlTextarea1 form-label fs-6 fw-bolder mb-3'>
                      Notes
                    </label>
                    <textarea
                      className='form-control form-control-lg form-control-solid '
                      id='exampleFormControlTextarea1'
                      rows={1}
                      name='note'
                      value={formik.values.note}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                  </div>
                </div>
                <div className='d-flex gap-5'>
                  <div className='mb-10 w-50'>
                    <label className='form-label fs-6 fw-bolder mb-3'>Users</label>
                    <Select
                      className='form-control form-control-lg form-control-solid '
                      placeholder='Users'
                      value={formik.values.users}
                      options={users}
                      onChange={(users) => {
                        formik.setFieldValue('users', users)
                      }}
                      isMulti
                    />
                  </div>
                  <div className='mb-10 w-50'>
                    <label className='form-label fs-6 fw-bolder mb-3'>Task Status</label>
                    <Select
                      className='form-control form-control-lg form-control-solid '
                      placeholder='Tasks Status'
                      value={formik.values.task_status}
                      options={status}
                      onChange={(task) => {
                        formik.setFieldValue('task_status', task)
                      }}
                    />
                  </div>
                </div>
                <button
                  type='submit'
                  disabled={isLoading || loadingUbdate}
                  className='btn btn-primary'
                >
                  {isLoading || loadingUbdate ? <ClipLoader color='#fff' size={20} /> : 'Submit'}
                </button>
              </form>
            </div>
          )}
        </div>
      </Modal>
    </>
  )
}

export default NewTasks
