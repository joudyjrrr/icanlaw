import React from 'react'
import {Button, Modal} from 'react-bootstrap'



const Test = () => {
  const [showA, setShowA] = React.useState(false)
 

  const handleCloseA = () => setShowA(false)
  const handleShowA = () => setShowA(true)

  return (
    <div className='App'>
      <header className='App-header'>
        <Button variant='primary' onClick={handleShowA}>
          Open A
        </Button>

        <Modal show={showA} onHide={handleCloseA}>
          <Modal.Header closeButton>
            <Modal.Title>In Modal A</Modal.Title>
          </Modal.Header>
        </Modal>
      </header>
    </div>
  )
}

export default Test
