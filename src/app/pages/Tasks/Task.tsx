import React, {useState} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import {KTSVG, toAbsoluteUrl} from '../../../_metronic/helpers'
import NewTasks from './NewTask'
import TaskApi from '../../../_metronic/API/Task/TaskApi'
import {ClipLoader} from 'react-spinners'
import Test from './Test'
import DeleteTask from './DeleteTask'
const Tasks = () => {
  const [open, setOpen] = useState(false)
  const [page, setPage] = useState<number>(0)
  const [id, setId] = useState<number>(0)
  const {data, isLoading, isPreviousData, refetch} = TaskApi.useQueryTasksAll(page)
  return (
    <div>
      <NewTasks open={open} setOpen={setOpen} id={id!} setId={setId}/>
      {/* <Test /> */}
      <PageTitle breadcrumbs={[]}>Tasks</PageTitle>
      <div className={`card `}>
        <div className='card-header border-0 pt-5'>
          <h3 className='card-title align-items-start flex-column'>
            <span className='text-muted mt-1 fw-semibold fs-7'>
              Number of Task <span className='mt-1 fw-bold '>{data?.meta?.total}</span>
            </span>
          </h3>
          <div
            className='card-toolbar'
            data-bs-toggle='tooltip'
            data-bs-placement='top'
            data-bs-trigger='hover'
            title='Click to add a user'
          >
            <a
              href='#'
              className='btn btn-primary'
              data-bs-toggle='modal'
              data-bs-target='#kt_modal_1'
              onClick={()=>setOpen(true)}
            >
              <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-3' />
              New Task
            </a>
          </div>
        </div>
    
      <div className='card-body py-3'>
        {isLoading ? (
          <div className='d-flex justify-content-center align-items-center mx-auto w-100'>
            <ClipLoader color='#36d7b7' size={80} />
          </div>
        ) : (
          <>
            {/* begin::Table container */}
            <div className='table-responsive'>
              <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                <thead>
                  <tr className='fw-bold text-muted text-center'>
                    <th className='min-w-140px'>Description</th>
                    <th className='min-w-140px'>Start Date</th>
                    <th className='min-w-120px'>End Date</th>
                    <th className='min-w-100px'>Notes</th>
                    <th className='min-w-100px'>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {data?.data && (
                    <>
                      {data.data.map((d: any) => (
                        <tr className='text-center'>
                          <td>{d.description}</td>
                          <td>{d.start_date}</td>
                          <td>{d.completion_date}</td>
                          <td>{d.note}</td>
                          <td>
                            <div className='d-flex justify-content-center flex-shrink-0'>
                              <a
                                href='#'
                                onClick={() => {
                                  setOpen(true)
                                  setId(d.id)
                                }}
                                className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1'
                              >
                                <KTSVG
                                  path='/media/icons/duotune/art/art005.svg'
                                  className='svg-icon-3'
                                />
                              </a>
                              <DeleteTask
                              to="tasks"
                                refetch={refetch}
                                id={id}
                                setId={() => setId(d?.id ?? '')}
                              />
                            </div>
                          </td>
                        </tr>
                      ))}
                    </>
                  )}
                </tbody>
              </table>
            </div>
          </>
        )}
      </div>
      <div className='d-flex justify-content-end m-5 gap-3'>
        <button
          disabled={page === 0}
          onClick={() => setPage(page - 1)}
          className='btn btn-sm btn-icon btn-bg-light btn-active-color-primary'
        >
          <KTSVG path='/media/icons/duotune/arrows/arr063.svg' className='svg-icon-2' />
        </button>
        <button
          onClick={() => setPage(page + 1)}
          disabled={
            page === (data?.meta?.total && Math.ceil(data?.meta?.total / 20) - 1) || isPreviousData
          }
          className='btn btn-sm btn-icon btn-bg-light btn-active-color-primary'
        >
          <KTSVG path='/media/icons/duotune/arrows/arr064.svg' className='svg-icon-2' />
        </button>
      </div>
    </div>
    </div>
  )
}

export default Tasks
