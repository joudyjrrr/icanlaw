import React, {useState} from 'react'
import {PageTitle} from '../../../../_metronic/layout/core'
import {KTSVG, toAbsoluteUrl} from '../../../../_metronic/helpers'
import CustAdjApi from '../../../../_metronic/API/Custmour/CustAdjApi'
import {ClipLoader} from 'react-spinners'
import DeleteTask from '../../Tasks/DeleteTask'
import AddCustAdj from './AddCustAdj'
const CustomerAdjectives = () => {
  const [open, setOpen] = useState(false)
  const [id, setId] = useState<number | undefined>()
  const {data, isLoading, refetch} = CustAdjApi.useQueryCustmourAdj()
  //   console.log(data)
  return (
    <div>
      <AddCustAdj open={open} setOpen={setOpen} id={id!} setId={setId} />
      <PageTitle breadcrumbs={[]}>CustomerAdjectives</PageTitle>
      <div className={`card `}>
        <div className='card-header border-0 pt-5'>
          <h3 className='card-title align-items-start flex-column'>
            <span className='text-muted mt-1 fw-semibold fs-7'>
              Number of Task <span className='mt-1 fw-bold '></span>
            </span>
          </h3>
          <div
            className='card-toolbar'
            data-bs-toggle='tooltip'
            data-bs-placement='top'
            data-bs-trigger='hover'
            title='Click to add a user'
          >
            <a
              href='#'
              className='btn btn-primary'
              data-bs-toggle='modal'
              data-bs-target='#kt_modal_1'
              onClick={()=>setOpen(true)}
            >
              <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-3' />
              New CustomerAdjectives
            </a>
          </div>
        </div>
        <div className='card-body py-3'>
          {isLoading ? (
            <div className='d-flex justify-content-center align-items-center mx-auto w-100'>
              <ClipLoader color='#36d7b7' size={80} />
            </div>
          ) : (
            <div className='table-responsive'>
              <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                <thead>
                  <tr className='fw-bold text-muted text-center'>
                    <th className='min-w-140px'>Name Arabic</th>
                    <th className='min-w-140px'>Name Englis</th>
                    <th className='min-w-120px'>Number Of Lawsuits</th>
                    <th className='min-w-100px'>User</th>
                    <th className='min-w-100px'>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {data.data && (
                    <>
                      {data.data.map((d: any) => (
                        <tr className='text-center'>
                          <td>{d.name_ar}</td>
                          <td>{d.name_en}</td>
                          <td>{d.number_of_lawsuits}</td>
                          <td>{d.user}</td>
                          <td>
                            <div className='d-flex justify-content-center flex-shrink-0'>
                              <a
                                href='#'
                                onClick={() => {
                                  setOpen(true)
                                  setId(d.id)
                                }}
                                className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1'
                              >
                                <KTSVG
                                  path='/media/icons/duotune/art/art005.svg'
                                  className='svg-icon-3'
                                />
                              </a>
                              <DeleteTask
                                to='customer-adjectives'
                                refetch={refetch}
                                id={id!}
                                setId={() => setId(d?.id ?? '')}
                              />
                            </div>
                          </td>
                        </tr>
                      ))}
                    </>
                  )}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default CustomerAdjectives
