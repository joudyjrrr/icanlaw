import {useFormik} from 'formik'
import React, {useEffect} from 'react'
import Modal from 'react-bootstrap/Modal'
import {IDataFormNames} from '../../../../_metronic/API/Custmour/type'
import CustApi from '../../../../_metronic/API/Custmour/CustAdjApi'
import {ClipLoader} from 'react-spinners'
import * as Yup from 'yup'
const AddCustAdj = ({
  open,
  setOpen,
  id,
  setId,
}: {
  open: boolean
  setOpen: (arg: boolean) => void
  id: number
  setId: (arg: number | undefined) => void
}) => {
  const Schema = Yup.object().shape({
    name_ar: Yup.string().required('name_ar is required'),
    name_en: Yup.string().required('name_en is required'),
  })
  const {data: CastmourData, isLoading: loadingData} = CustApi.useQueryCustAdjById(id)
  useEffect(() => {
    if (CastmourData) {
      formik.setFieldValue('name_en', CastmourData.name_en)
      formik.setFieldValue('name_ar', CastmourData.name_ar)
    }
    if(!id){
        formik.setFieldValue('name_en', "")
        formik.setFieldValue('name_ar',"")
    }
  }, [CastmourData , id])
  const {mutate, isLoading} = CustApi.NewCustmourAdjQuery()
  const {mutate: Ubdate, isLoading: loadingUbdate} = CustApi.UbdateCustmourAdjQuery()
  const {...formik} = useFormik<IDataFormNames>({
    initialValues: {
      name_ar: '',
      name_en: '',
    },
    validationSchema: Schema,
    onSubmit: async (values, {resetForm}) => {
      console.log(values)
      if (id) {
        Ubdate(
          {
            id: id,
            name_ar: values.name_ar,
            name_en: values.name_en,
          },
          {
            onSuccess: () => {
              setOpen(false)
              resetForm()
            },
          }
        )
      } else {
        mutate(
          {
            name_ar: values.name_ar,
            name_en: values.name_en,
          },
          {
            onSuccess: () => {
              setOpen(false)
              resetForm()
            },
          }
        )
      }
    },
  })
  return (
    <Modal
      show={open}
      onHide={() =>{ setOpen(false); setId(undefined)} }
      style={{
        marginTop: '130px',
      }}
    >
      <Modal.Header closeButton>
        <Modal.Title>New CustomerAdjectives </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        {loadingData ? (
          <div className='d-flex justify-content-center align-items-center mx-auto w-100'>
            <ClipLoader color='#36d7b7' size={70} />
          </div>
        ) : (
          <form onSubmit={formik.handleSubmit}>
            <div className='d-flex gap-5 justify-content-center mx-auto'>
              <div className='mb-10'>
                <label className='form-label'>Name In English</label>
                <input
                  type='text'
                  name='name_en'
                  className='form-control'
                  value={formik.values.name_en}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.name_en && formik.errors.name_en && (
                  <div className='text-danger'>{formik.errors.name_en}</div>
                )}
              </div>
              <div className='mb-10'>
                <label className='form-label'>Name In Arabic</label>
                <input
                  type='text'
                  name='name_ar'
                  className='form-control'
                  value={formik.values.name_ar}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.name_ar && formik.errors.name_ar && (
                  <div className='text-danger'>{formik.errors.name_ar}</div>
                )}
              </div>
            </div>
            <button type='submit' disabled={isLoading} className='btn btn-primary'>
              {isLoading || loadingUbdate ? <ClipLoader color='#fff' size={20} /> : 'Submit'}
            </button>
          </form>
        )}
      </Modal.Body>
    </Modal>
  )
}

export default AddCustAdj
