import React, {useMemo, useState} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import CutApi from '../../../_metronic/API/Custmour/CutApi'
import {KTSVG, toAbsoluteUrl} from '../../../_metronic/helpers'
import {ClipLoader} from 'react-spinners'

const Customer = () => {
  const [showHiddenRow, setShowHiddenRow] = useState(false)
  const {data, isLoading} = CutApi.useQueryCustmour()
  const toggleHiddenRow = () => {
    setShowHiddenRow(!showHiddenRow)
  }

  return (
    <div>
      <PageTitle breadcrumbs={[]}>Customers</PageTitle>
      <div className='card'>
        <div className='card-header border-0 pt-5'>
          <h3 className='card-title align-items-start flex-column'>
            <span className='text-muted mt-1 fw-semibold fs-7'>
              Number of Task <span className='mt-1 fw-bold'></span>
            </span>
          </h3>
        </div>
        <div className='card-body py-3'>
          <div className='table-responsive'>
            {isLoading ? (
              <div className='d-flex justify-content-center align-items-center mx-auto w-100'>
                <ClipLoader color='#36d7b7' size={80} />
              </div>
            ) : (
              <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                <thead>
                  <tr className='fw-bold text-muted text-center'>
                    <th>#</th>
                    <th className='min-w-140px'>Name Arabic</th>
                    <th className='min-w-140px'>Name Englis</th>
                    <th className='min-w-120px'>Username</th>
                    <th className='min-w-100px'>Mobile Number</th>
                    <th className='min-w-100px'>Nationality</th>
                  </tr>
                </thead>

                <tbody>
                  {data?.data && (
                    <>
                      {data.data?.map((d: any, index: number) => (
                        <tr className='text-center' key={index}>
                          <td>
                            {!showHiddenRow ? (
                              <button
                                onClick={toggleHiddenRow}
                                className='btn btn-sm btn-icon btn-bg-light btn-active-color-primary'
                              >
                                <KTSVG
                                  path='/media/icons/duotune/arrows/arr065.svg'
                                  className='svg-icon-2'
                                />
                              </button>
                            ) : (
                              <button
                                onClick={toggleHiddenRow}
                                className='btn btn-sm btn-icon btn-bg-light btn-active-color-primary'
                              >
                                <KTSVG
                                  path='/media/icons/duotune/arrows/arr062.svg'
                                  className='svg-icon-2'
                                />
                              </button>
                            )}
                          </td>
                          <td>{d.name_ar}</td>
                          <td>{d.name_en}</td>
                          <td>{d.username}</td>
                          <td>{d.mobile_no}</td>
                          <td>{d.nationality}</td>
                        </tr>
                      ))}
                      {showHiddenRow &&
                        data.data.map((d: any, index: number) => (
                          <tr className='text-center' key={index} style={{background: '#80808045'}}>
                            <td></td>
                            <td>{d.address}</td>
                            <td>{d.user}</td>
                            <td>{d.number_of_lawsuits}</td>
                            <td>{d.user}</td>
                            <td></td>
                          </tr>
                        ))}
                    </>
                  )}
                </tbody>
              </table>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Customer
