import React, {useState} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import {KTSVG} from '../../../_metronic/helpers'
import CustAdjApi from '../../../_metronic/API/Custmour/CustAdjApi'
import AddUser from './AddUser'
import UserApi from '../../../_metronic/API/User/UserApi'
import DeleteTask from '../Tasks/DeleteTask'
const Users = () => {
  const [open, setOpen] = useState(false)
  const [id, setId] = useState<number>(0)
  const {data: users, refetch} = UserApi.useQueryUsersAll()
  console.log(users?.data)
  return (
    <div>
      <AddUser open={open} setOpen={setOpen} id={id} setId={setId} />
      <PageTitle breadcrumbs={[]}>Users</PageTitle>
      <div className={`card `}>
        <div className='card-header border-0 pt-5'>
          <h3 className='card-title align-items-start flex-column'>
            <span className='text-muted mt-1 fw-semibold fs-7'>
              Number of Users <span className='mt-1 fw-bold '></span>
            </span>
          </h3>
          <div
            className='card-toolbar'
            data-bs-toggle='tooltip'
            data-bs-placement='top'
            data-bs-trigger='hover'
            title='Click to add a user'
          >
            <a href='#' className='btn btn-primary' onClick={() => setOpen(true)}>
              <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-3' />
              New User
            </a>
          </div>
        </div>
        <div className='card-body py-3'>
          <div className='table-responsive'>
            <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
              <thead>
                <tr className='fw-bold text-muted text-center'>
                  <th className='min-w-140px'>Name</th>
                  <th className='min-w-100px'>Email</th>
                  <th className='min-w-100px'>Permission</th>
                  <th className='min-w-100px'>Actions</th>
                </tr>
              </thead>
              <tbody>
                {users?.data && (
                  <>
                    {users?.data.map((d: any) => (
                      <tr className='text-center'>
                        <td>{d.name}</td>
                        <td>{d.email}</td>
                        <td>{d.permission}</td>
                        <td>
                          <div className='d-flex justify-content-center flex-shrink-0'>
                            <a
                              href='#'
                              onClick={() => {
                                setOpen(true)
                                setId(d.id)
                              }}
                              className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1'
                            >
                              <KTSVG
                                path='/media/icons/duotune/art/art005.svg'
                                className='svg-icon-3'
                              />
                            </a>
                            <DeleteTask
                              to='users'
                              refetch={refetch}
                              id={id!}
                              setId={() => setId(d?.id ?? '')}
                            />
                          </div>
                        </td>
                      </tr>
                    ))}
                  </>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Users
