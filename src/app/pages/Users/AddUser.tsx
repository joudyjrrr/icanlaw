import React, {useEffect, useState} from 'react'
import {Button, InputGroup, Modal} from 'react-bootstrap'
import {IUserForm} from '../../../_metronic/API/User/type'
import {useFormik} from 'formik'
import * as Yup from 'yup'
import {HiEye, HiEyeOff} from 'react-icons/hi'
import './Password.css'
import Select from 'react-select'
import UserApi from '../../../_metronic/API/User/UserApi'
import {ClipLoader} from 'react-spinners'
const AddUser = ({
  open,
  setOpen,
  id,
  setId,
}: {
  open: boolean
  setOpen: (arg: boolean) => void
  id: number
  setId: (arg: number) => void
}) => {
  const {data: roles} = UserApi.useQueryRoles()
  const {data: UsersData} = UserApi.useQueryUserById(id!)
  console.log(UsersData?.user)
  useEffect(() => {
    if (UsersData) {
      formik.setFieldValue('name', UsersData.user.name)
      formik.setFieldValue('email', UsersData.user.email)
      formik.setFieldValue('username', UsersData.user.username)
      formik.setFieldValue('role', {
        value: UsersData.roles.id,
        label: UsersData.roles.name,
      })
    }
  }, [UsersData])
  const [showPassword, setShowPassword] = useState(false)
  const [showConfirmationPassword, setShowConfirmationPassword] = useState(false)

  const togglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword)
  }

  const toggleConfirmationPasswordVisibility = () => {
    setShowConfirmationPassword((prevShowConfirmationPassword) => !prevShowConfirmationPassword)
  }
  const {mutate, isLoading} = UserApi.NewUserQuery()
  const Schema = Yup.object().shape({
    email: Yup.string().email('Wrong email format').required('Email is required'),
    name: Yup.string().required('name is required'),
    username: Yup.string().required('username is required'),
    password: Yup.string().min(9, 'Min Charechter is 9').required('password is required'),
    password_confirmation: Yup.string()
      .min(9, 'Min Charechter is 9')
      .required('password is required'),
  })
  const {...formik}: any = useFormik<IUserForm>({
    initialValues: {
      email: '',
      name: '',
      username: '',
      password: '',
      password_confirmation: '',
      role: {label: '', value: null},
    },
    validationSchema: Schema,
    onSubmit: async (values, {resetForm}) => {
      console.log(values)
      mutate(
        {
          email: values.email,
          name: values.name,
          username: values.username,
          password: values.password,
          password_confirmation: values.password_confirmation,
          role_id: values.role.value!,
        },
        {
          onSuccess: () => {
            setOpen(false)
            resetForm()
          },
        }
      )
    },
  })
  return (
    <div>
      <Modal show={open} onHide={() => setOpen(false)}>
        <Modal.Header closeButton>
          <Modal.Title>New User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={formik.handleSubmit}>
            <div className='d-flex gap-5 justify-content-center mx-auto'>
              <div className='mb-10'>
                <label className='form-label fs-6 fw-bolder mb-3'>Name</label>
                <input
                  placeholder='Name'
                  type='text'
                  name='name'
                  className='form-control form-control-lg form-control-solid '
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.name && formik.errors.name && (
                  <div className='text-danger'>{formik.errors.name}</div>
                )}
              </div>
              <div className='mb-10'>
                <label className='form-label fs-6 fw-bolder mb-3'>Username</label>
                <input
                  placeholder='Username'
                  type='text'
                  name='username'
                  className='form-control form-control-lg form-control-solid '
                  value={formik.values.username}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.username && formik.errors.username && (
                  <div className='text-danger'>{formik.errors.username}</div>
                )}
              </div>
            </div>
            <div className='d-flex gap-5 justify-content-center mx-auto'>
              <div className='mb-10'>
                <label className='form-label fs-6 fw-bolder mb-3'>Email</label>
                <input
                  placeholder='Email'
                  type='email'
                  name='email'
                  className='form-control form-control-lg form-control-solid '
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.email && formik.errors.email && (
                  <div className='text-danger'>{formik.errors.email}</div>
                )}
              </div>
              <div className='mb-10 r'>
                <label className='form-label fs-6 fw-bolder mb-3'>Password</label>
                <input
                  placeholder='Password'
                  type={showPassword ? 'text' : 'password'}
                  name='password'
                  className='form-control form-control-lg form-control-solid '
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <p onClick={togglePasswordVisibility}>
                  {' '}
                  {showPassword ? <HiEyeOff className='icon' /> : <HiEye className='icon' />}
                </p>
                {formik.touched.password && formik.errors.password && (
                  <div className='text-danger'>{formik.errors.password}</div>
                )}
              </div>
            </div>
            <div className='d-flex gap-5 justify-content-center mx-auto'>
              <div className='mb-10 r'>
                <label className='form-label fs-6 fw-bolder mb-3'>Confirm Password</label>
                <input
                  placeholder='Confirm Password'
                  type={showConfirmationPassword ? 'text' : 'password'}
                  name='password_confirmation'
                  className='form-control form-control-lg form-control-solid '
                  value={formik.values.password_confirmation}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <p onClick={toggleConfirmationPasswordVisibility}>
                  {' '}
                  {showConfirmationPassword ? (
                    <HiEyeOff className='icon' />
                  ) : (
                    <HiEye className='icon' />
                  )}
                </p>
                {formik.touched.password_confirmation && formik.errors.password_confirmation && (
                  <div className='text-danger'>{formik.errors.password_confirmation}</div>
                )}
              </div>
              <div className='mb-10 w-50'>
                <label className='form-label fs-6 fw-bolder mb-3'>Roles</label>
                <Select
                  className='form-control form-control-lg form-control-solid '
                  placeholder='Roles'
                  value={formik.values.role}
                  options={roles}
                  onChange={(role) => {
                    formik.setFieldValue('role', role)
                  }}
                />
              </div>
            </div>
            <button type='submit' className='btn btn-primary'>
              {isLoading ? <ClipLoader color='#fff' size={20} /> : 'Submit'}
            </button>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  )
}

export default AddUser
