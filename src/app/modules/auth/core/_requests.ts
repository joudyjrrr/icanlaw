import axios from 'axios'
import { AuthModel, UserModel } from './_models'
import DeliveryApiInstances from '../../../../_metronic/API/axios'

const API_URL = process.env.REACT_APP_API_URL

export const GET_USER_BY_ACCESSTOKEN_URL = `${API_URL}/verify_token`
export const LOGIN_URL = `/login`
export const REGISTER_URL = `${API_URL}/register`
export const REQUEST_PASSWORD_URL = `${API_URL}/forgot_password`
export type ILogin = {
  email: string,
  password: string
}
export interface IUser {
 id: number;
 name : string;
 email : string;
}
export interface IRespone  {
  access_token : AuthModel;
  message : string;
  result : boolean;
  user : IUser
}
// Server should return AuthModel
export function login(data: ILogin) {
  return DeliveryApiInstances.post<any>(LOGIN_URL, {
    ...data
  })
}

// Server should return AuthModel
export function register(
  email: string,
  firstname: string,
  lastname: string,
  password: string,
  password_confirmation: string
) {
  return axios.post(REGISTER_URL, {
    email,
    first_name: firstname,
    last_name: lastname,
    password,
    password_confirmation,
  })
}

// Server should return object => { result: boolean } (Is Email in DB)
export function requestPassword(email: string) {
  return axios.post<{ result: boolean }>(REQUEST_PASSWORD_URL, {
    email,
  })
}

export function getUserByToken(token: string) {
  return axios.post<UserModel>(GET_USER_BY_ACCESSTOKEN_URL, {
    api_token: token,
  })
}

